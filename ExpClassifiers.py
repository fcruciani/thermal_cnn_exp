#ExpClassifiers module
#Author: Federico Cruciani
#Description: Containes classes implementing CNN module with 1 and 5 channels.
from keras.models import Sequential
from keras.layers import *
import keras
from ExpBaseClassifier import ExpBaseClassifier

#Simple classifier Single thermal image (1 channel)
class HAR_CNN_thermal(ExpBaseClassifier):
	def __init__(self,patience,layers=3,kern_size=(2,2),num_classes=4,divide_kernel_size=False,fontSize=16,suffix=""):
		self.name = str(layers)+"-CNN_"+str(num_classes)+"cl_"+suffix
		super().__init__(self.name,patience,fontSize)
		self.model = Sequential()
		filters = 12
		self.model.add( Conv2D(filters,input_shape=(31,32,1),kernel_size=kern_size,padding='same',activation='relu', name="layer_1") )
		self.model.add( Dropout(0.2) )
		#No max pooling: images are low resolution
		#self.model.add(MaxPooling2D())
		for i in range(2,layers+1):
			filters = filters*2
			if divide_kernel_size:
				kern_size = int(kern_size / 2)
			layer_name = "layer_"+str(i)
			self.model.add( Conv2D(filters,kernel_size=kern_size,padding='same',activation='relu', name=layer_name) )
			self.model.add( Dropout(0.2) )
			#self.model.add(MaxPooling2D())
		#Automatic features
		self.model.add(Flatten(name="automatic_features"))
		self.model.add( Dense(64,activation='relu', name="layer_dense") )
		self.model.add( Dense(num_classes,activation='softmax',  name="output_layer"))
		self.model.compile( loss='mse',metrics=['mse','acc'], optimizer='sgd' )
		self.model.summary()
		self.name2layer = {}
		for layer in self.model.layers:
			self.name2layer[layer.name] = layer

	def fit_gen(self,epochs, val_data, img_bg):
		spe = img_bg.get_steps_per_epoch()
		self.history = self.model.fit_generator(img_bg.batch_generator(epochs=epochs),epochs=epochs,steps_per_epoch=spe,validation_data=val_data,callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger])

#CNN classifier 5 channels (5 thermal images)
class HAR_CNN_thermal_5ch(ExpBaseClassifier):
	def __init__(self,patience,layers=3,kern_size=(2,2),num_classes=4,divide_kernel_size=False,fontSize=16,suffix=""):
		self.name = str(layers)+"-CNN_"+str(num_classes)+"cl_"+suffix
		super().__init__(self.name,patience,fontSize)
		self.model = Sequential()
		filters = 12
		self.model.add( Conv2D(filters,input_shape=(31,32,5),kernel_size=kern_size,padding='same',activation='relu', name="layer_1") )
		self.model.add( Dropout(0.2) )
		#self.model.add(MaxPooling2D())
		for i in range(2,layers+1):
			filters = filters*2
			if divide_kernel_size:
				kern_size = int(kern_size / 2)
			layer_name = "layer_"+str(i)
			self.model.add( Conv2D(filters,kernel_size=kern_size,padding='same',activation='relu', name=layer_name) )
			self.model.add( Dropout(0.2) )
			#self.model.add(MaxPooling2D())
		#Automatic features
		self.model.add(Flatten(name="automatic_features"))
		#for multilabel DO NOT use softmax use sigmoid
		self.model.add( Dense(64,activation='relu', name="layer_dense") )
		self.model.add( Dense(num_classes,activation='softmax',  name="output_layer"))
		self.model.compile( loss='mse',metrics=['mse','acc'], optimizer='sgd' )
		self.model.summary()
		self.name2layer = {}
		for layer in self.model.layers:
			self.name2layer[layer.name] = layer

	def fit_gen(self,epochs, val_data, img_bg):
		spe = img_bg.get_steps_per_epoch()
		self.history = self.model.fit_generator(img_bg.batch_generator(epochs=epochs),epochs=epochs,steps_per_epoch=spe,validation_data=val_data,callbacks = [self.logger, self.early_stopping, self.checkpoint, self.csv_logger])
