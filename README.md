# thermal_cnn_exp
CNN classifier using thermal images.

#Dependencies
Main Dependencies: numpy, pandas, sklearn, keras.

#Configuration:
edit datapath variable in 'img_batch_generator.py' with the 
actual path of the dataset.


#Running
launch 'test_model.py' to evaluate trained model:
	- this uses weights saved in 
	  './keras_logs/3-CNN_5cl_5_chweights_best.hdf5'

launch 'train_model.py' to retrain the model.
	- this will erase last saved model and produce a 
	  new set of weights saved in 
	  './keras_logs/3-CNN_5cl_5_chweights_best.hdf5'

#NOTE: a pre-trained model is available in 'keras_logs'.


