# thermal_cnn_exp
CNN classifier using thermal images.


launch 'test_model.py' to evaluate trained model:
	- this uses weights saved in 
	  './keras_logs/3-CNN_5cl_5_chweights_best.hdf5'

launch 'train_model.py' to retrain the model.
	- this will erase last saved model and produce a 
	  new set of weights saved in 
	  './keras_logs/3-CNN_5cl_5_chweights_best.hdf5'

#NOTE: a pre-trained model is available in 'keras_logs'.

