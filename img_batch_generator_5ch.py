#img_batch_generator module
#Author: Federico Cruciani
#Description: this module implements batch_generator 
#			  functiopnalities to train and validate the classifier.
#             Each training mini batch contains 1 random frame for each of 
#				the 5 simultaneous viewpoints.
#             batch size 5x31x32x5 = (numClasses)x(ImgWidth)x(ImgHeight)x(NumViewpoints)
import pandas as pd 
import numpy as np 
from scipy import misc 
from random import randint
import glob
import keras.utils

activities = ['ArmForward','ArmSide','ArmsDown','Bend','Sitting']

viewpoints = ['Ceiling','Corner1','Corner2','Corner3','Corner4']

#Change this with your actual data path
datapath = "../../../Thermal/Dataset/"

#500 dataset size. 50 as validation. 450 samples for training
def get_steps_per_epoch():
	return 450

def batch_generator(epochs):
	for epochs in range(epochs):
		#Get a random sample between 0-450
		for i in range(450):
			# tensor shape 31x32x5 5=viewpoints as channel
			# 1 batch = num of activities (5) x tensor shape => 5x31x32x5
			X_batch = np.zeros((len(activities),31,32,len(viewpoints)))
			y_batch = []
			for index,activity in enumerate(activities):
				X_point = np.zeros((31,32,len(viewpoints)))
				rnd = randint(0,450)
				for ch,viewpoint in enumerate(viewpoints):
					files = glob.glob(datapath+viewpoint+"/"+activity+"/*.png")
					img = misc.imread(files[rnd])
					X_point[:,:,ch] = img
				X_batch[index,:,:,:] = X_point
				y_batch.append(index)
			#Normalizing images
			X_train_batch = X_batch.astype('float32')
			X_train_batch /= 255.
			y_es = np.zeros(len(y_batch))
			y_es[:] = [y for y in y_batch]
			y_es_scaled  = keras.utils.to_categorical(y_es, num_classes=5)
			yield (X_train_batch,y_es_scaled) 

#Using last 50 samples as validation
#TODO: k-fold validation
def get_validation_set():
	viewpoint = 'Corner1'
	X_test = np.zeros((250,31,32,5))
	#31,32,5 1 data point
	#500 poses per activity
	#50 x 5 poses per activity of test
	y_test = []
	num_points = 0
	for iterator in range(450,500):
		X_batch = np.zeros((31,32,5))
		for index,activity in enumerate(activities):
			for ch,viewpoint in enumerate(viewpoints):
				files = glob.glob(datapath+viewpoint+"/"+activity+"/*.png")
				img = misc.imread(files[iterator])
				X_batch[:,:,ch] = img
			X_test[num_points,:,:,:] = X_batch
			y_test.append(index)
			num_points += 1

	#Normalizing image
	X_test_batch = X_test.astype('float32')
	X_test_batch /= 255.
	y_es = np.zeros(len(y_test))
	y_es[:] = [y for y in y_test]
	y_es_scaled  = keras.utils.to_categorical(y_es, num_classes=5)
	return X_test_batch,y_es_scaled