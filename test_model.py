import numpy as np 
from scipy import misc
import glob
import img_batch_generator_5ch as img_bg
import ExpClassifiers as Classifiers
from sklearn.metrics import classification_report

classes = ['ArmForward','ArmsDown','ArmSide','Bend','Sitting']

viewpoints = ['Ceiling','Corner1','Corner2','Corner3','Corner4']

datapath = "/home/fedecrux/ownCloud/Documents/Python/Thermal/Dataset/"


clf = Classifiers.HAR_CNN_thermal_5ch(patience=10,num_classes=5,suffix="5_ch")
#Load learned weights
clf.loadBestWeights()
#Load validation set
(X_vld, y_vld) = img_bg.get_validation_set()
#get predictions on validation set
predictions = clf.predict(X_vld,batch_size=1)
predictions_inv = [ [np.argmax(x)] for x in predictions]
y_vld_inv = [ [np.argmax(y)] for y in y_vld]

clf.printConfusionMatrix(true=y_vld_inv,pred=predictions_inv,classes=classes)

cr = classification_report(np.array(y_vld_inv), np.array(predictions_inv),target_names=classes,digits=5)
print(cr)
