import numpy as np 
from scipy import misc
import glob
import img_batch_generator_5ch as img_bg
import ExpClassifiers as Classifiers

activities = ['ArmForward','ArmsDown','ArmSide','Bend','Sitting']

viewpoints = ['Ceiling','Corner1','Corner2','Corner3','Corner4']

datapath = "/home/fedecrux/ownCloud/Documents/Python/Thermal/Dataset/"



clf = Classifiers.HAR_CNN_thermal_5ch(patience=10,num_classes=5,suffix="5_ch")
#get validation set
(X_vld, y_vld) = img_bg.get_validation_set()
clf.fit_gen( epochs=5000, val_data=(X_vld, y_vld), img_bg=img_bg )
